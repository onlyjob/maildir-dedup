# NAME

maildir-dedup - find and remove duplicates in Maildir subfolders

# SYNOPSIS

maildir-dedup "**infile**" "**\[infile\]**"
maildir-dedup "**dir**" "**\[dir\]**"

# DESCRIPTION

maildir-dedup finds and (optionally) delete duplicated mail
in Maildir subfolders by trying to unify (or ignore) differences
in headers and comparing sha256 digests of email files.

Known headers added by spam detection software are ignored as well as metadata
headers added by Kmail.

maildir-dedup attempts to correct header's case, spacing and unify encodings before
calculating sha256 of the whole email file including headers.

Please note that validation is not performed nor any attempt to determine "better"
emails. Email file found first is never deleted.

Preference over files to delete can be controlled by the order of directories in
command line (e.g. "maildir-dedup cur new" to prefer removals from "new" directory).

# OPTIONS

- **-h**, **--help**

        Print a brief help message and exit.

- **--man**

        Print embedded manual.

- **--copy=****DIR**

        Copy duplicate files to given directory.

- **--delete**

        Delete all duplicates as they a found.

- **--delete-later**, **--delete-last**

        Delete all duplicates after all files are scanned.
        This mode is preferable when email software is running to avoid triggering rescan
        more than once.

- **--tests**, **--qa**

        Run embedded QA tests and exit.

- **--dump**

        Dump processed email to the output and stop. Useful for debugging.

- **--verbose**, **-v**

        Print some rarely needed debugging information.

# AUTHOR

Dmitry Smirnov &lt;onlyjob@raid6.com.au>

# COPYRIGHT

Copyright: 2016 Libre Solutions Pty Ltd (http://raid6.com.au)

# LICENSE

License: GPL-3+

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This package is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

# DONATIONS

    Donations are much appreciated, please consider donating:

      Bitcoins : 15nCM6Rs4zoQKhBV55XxcPfwPKeAEHPbAn
     Litecoins : LZMLDNSfy3refx7bKQtEvPyYSbNHsfzLRZ
    AUD/PayPal : https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=onlyjob%40gmail%2ecom&lc=AU&currency_code=AUD

# AVAILABILITY

https://gitlab.com/onlyjob/maildir-dedup

# VERSION

2016.0229
