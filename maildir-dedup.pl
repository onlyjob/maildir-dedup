#!/usr/bin/perl
=head1 NAME

maildir-dedup - find and remove duplicates in Maildir subfolders

=head1 SYNOPSIS

maildir-dedup "B<infile>" "B<[infile]>"
maildir-dedup "B<dir>" "B<[dir]>"

=head1 DESCRIPTION

maildir-dedup finds and (optionally) delete duplicated mail
in Maildir subfolders by trying to unify (or ignore) differences
in headers and comparing sha256 digests of email files.

Known headers added by spam detection software are ignored as well as metadata
headers added by Kmail.

maildir-dedup attempts to correct header's case, spacing and unify encodings before
calculating sha256 of the whole email file including headers.

Please note that validation is not performed nor any attempt to determine "better"
emails. Email file found first is never deleted.

Preference over files to delete can be controlled by the order of directories in
command line (e.g. "maildir-dedup cur new" to prefer removals from "new" directory).

=head1 OPTIONS

=over 8

=item B<-h>, B<--help>

 Print a brief help message and exit.

=item B<--man>

 Print embedded manual.

=item B<--copy=>B<DIR>

 Copy duplicate files to given directory.

=item B<--delete>

 Delete all duplicates as they a found.

=item B<--delete-later>, B<--delete-last>

 Delete all duplicates after all files are scanned.
 This mode is preferable when email software is running to avoid triggering rescan
 more than once.

=item B<--tests>, B<--qa>

 Run embedded QA tests and exit.

=item B<--dump>

 Dump processed email to the output and stop. Useful for debugging.

=item B<--verbose>, B<-v>

 Print some rarely needed debugging information.

=back

=head1 AUTHOR

Dmitry Smirnov <onlyjob@raid6.com.au>

=head1 COPYRIGHT

Copyright: 2016 Libre Solutions Pty Ltd (http://raid6.com.au)

=head1 LICENSE

License: GPL-3+

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

=head1 DONATIONS

 Donations are much appreciated, please consider donating:

   Bitcoins : 15nCM6Rs4zoQKhBV55XxcPfwPKeAEHPbAn
  Litecoins : LZMLDNSfy3refx7bKQtEvPyYSbNHsfzLRZ
 AUD/PayPal : https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=onlyjob%40gmail%2ecom&lc=AU&currency_code=AUD

=head1 AVAILABILITY

https://gitlab.com/onlyjob/maildir-dedup

=head1 VERSION

2016.0229

=cut
use 5.014;
use strict;
use warnings FATAL => 'all';
binmode STDERR, ':encoding(UTF-8)';
use autouse 'Data::Dumper'=>qw(Dumper);
use autouse 'Pod::Usage'=>qw(pod2usage);
use Getopt::Long;

use File::Basename;
use File::Copy;
use File::Find qw(finddepth find);
use Text::Iconv;
use MIME::Base64;
use MIME::QuotedPrint;
use Digest::SHA qw(sha256 sha256_hex sha256_base64);

my %OPT=(
);
GetOptions( \%OPT,
    ,'help|h|?' ,'man'
    ,'delete'
    ,'delete-later|delete-last'
    ,'dump'
    ,'qa|tests'
    ,'copy=s'
    ,'verbose|v'
) or pod2usage(0);
pod2usage(1) if ($OPT{'help'});
pod2usage(-exitstatus => 0, -verbose => 2) if ($OPT{ 'man' });

if ($OPT{'qa'}){
    tests();
    exit;
}

my %sha;
my %dellater;

my $stat_files=0;
my $stat_files_ln=0;
my $stat_dups=0;
my $stat_deleted=0;

## calculate hashes:
print STDERR ":: calculating hashes: ";
while (my $file = shift @ARGV){
    next if -p $file or -S _ or -b _ or -c _;  # (named pipe, socket, block special file or character special file);
    next if -l $file;           # skip symbolic links.

    if (-d $file){
        finddepth (         ## Expand directory into list of files:
            {   no_chdir => 1,
                wanted => sub {
                    return if $file eq $File::Find::name;           # skip top-level directory.
                    return if $File::Find::name eq '.';             # skip pseudo-dir.
                    if ( -f $File::Find::name ){
                        push @ARGV, $File::Find::name;
                    }
                }
            }, $file
        );
        next;
    }

    print STDERR "\b" x $stat_files_ln;
    warn "$file\n"  if $OPT{'verbose'};
    $stat_files += 1;
    $stat_files_ln = length($stat_files);
    print STDERR $stat_files;

    if (open my $FH, '<', $file){
        my $body = do { local($/); <$FH> };           # Slurp.
        close $FH;
        $_ = filter($body);
        if ($OPT{'dump'}){
            say $_;
            exit;
        }
        my $digest = sha256_base64($_);
        if (defined $sha{$digest}){
            ## Duplicate found
            $stat_dups += 1;
            my $fileN = $sha{$digest};
            warn "\nDuplicates:\n    $fileN\n    $file\n";
            if ($OPT{'copy'}){
                copy($fileN, $OPT{'copy'}.'/'.basename($fileN))
                  or die "Unable to copy " .$OPT{'copy'}.'/'.basename($fileN);

                copy($file, $OPT{'copy'}.'/'.basename($file))
                  or die "Unable to copy " .$OPT{'copy'}.'/'.basename($file);
            }
            if ($OPT{'delete'} and -s $fileN){   ## safeguard: original file must exist when duplicate is deleted.
                warn "Removing $file\n";
                unlink $file
                  and $stat_deleted += 1;
                $stat_files -= 1;
            }
            elsif ($OPT{'delete-later'}){
                $dellater{$file} = $fileN;
            }
        }
        else{
            $sha{$digest} = $file;
        }
    }
}
say STDERR q{};

while (my ($file, $fileN) = each %dellater){
    warn "Removing $file\n";
    if (-s $fileN){
        unlink $file
          and $stat_deleted += 1;
    }
}

say STDERR ":: $stat_dups duplicates found.";
say STDERR ":: $stat_deleted file(s) deleted." if ($OPT{'delete'} or $OPT{'delete-later'});

exit;

sub filter {

    state %headers_case;
    %headers_case = qw(
        Content-type                Content-Type
        Content-transfer-encoding   Content-Transfer-Encoding
        Mime-Version                MIME-Version
        Message-ID                  Message-Id
        CC                          Cc
        Reply-to                    Reply-To
        MIME-version                MIME-Version
        FROM                        From
        TO                          To
        In-reply-To                 In-Reply-To
    );

    state @headers_drop_ifempty;
    @headers_drop_ifempty = qw(
        X-Debian-PR-Keywords
        X-MS-Has-Attach
        X-MS-TNEF-Correlator
    );
    
    state @headers_drop;
    @headers_drop = qw(
        X-Bogosity
        X-BSFilter-Flag
        X-BSFilter-Probability
        X-Spam-Report
        X-Spam-Status
        X-Spam-Flag
        X-Spam-Checker-Version
        X-Spam-Level
        MIME-Version

        X-Length
        Status
        X-Status
        X-KMail-EncryptionState
        X-KMail-SignatureState
        X-KMail-MDN-Sent
        X-UID
    );
    
    local $_ = shift;

    s{[ ]*\n[ \t]+}{ }msg;
    s{\n+(\-\-\S)}{\n\n$1}msg;
    s{\s+\Z}{}smg;

    # malformed header?
    # q{From JobSearchInformant@Headhunter.net  Sun Oct 21 10:34:47 2001}
    s{\AFrom[ ]+[^\n]+\d+\n}{}sm;

    ## case corrections
    for my $h (keys %headers_case){
        s{^$h:}{$headers_case{$h}:}msg;
    }

    ## drop (some) empty headers
    for my $h (@headers_drop_ifempty){
        s{^$h:[ ]*\n}{}ms;
    }

    ## drop some non-empty headers
    for my $h (@headers_drop){
        s{^$h:[^\n]+\n}{}ms;
    }

    ## Content-Type
    s{^Content\-Type:\s\K([^\n]+)}{
      eval{
            local $_=$1;

            ## case:
            s{Boundary=}{boundary=};
            s{Charset=}{charset=};
            s{Text/Plain}{text/plain};

            ## quote:
            s{;\K\s*}{ }msg;                # Un-squash ";" separated parameters.
            tr/";//d;

            ## reorder fields:
            my @f = m{(\w+[=/]\S+)}msg;
            $_ = join q{; }, sort @f;

            return $_;
          };
    }smge;

    ## Content-Disposition
    s{^Content\-Disposition:\s\K([^\n]+)}{
      eval{
            local $_=$1;

            ## reorder fields:
            #~ my @f = m{(\w+[=]"[^"]+")}msg;
            #~ $_ = join q{; }, sort @f;

            tr/";//d;                # Un-quote, drop some chars.

            return $_;
          };
    }smge;

    ## From|To|Subject
    s{^(?:From|To):\K(\S)}{ $1}msg;          # inject missing space after header.

    s{^(?:From|To|Cc|Bcc|Subject|Reply-To|Return-Path|Return-Receipt-To|Disposition-Notification-To|Thread-Topic):\s\K([^\n]+)}{
      eval{
        local $_ = $1;
        s{\?=\K\s+(=\?)}{$1}smg;    ## merge encoded chunks
        $_=transcod($_);
        tr/_/ /;                    ## replace underscores with spaces (transcoding error?).

        s{^\s+}{}sm;                # drop leading spaces.
        tr/<>",//d;                 # drop some characters (quotes, etc.).
        s{[ ]+}{ }smg;              # squash spaces

        return $_;
      }
    }smgei;

    ## Sender
    ## quote sender
    s{^Sender:\s\K([^\n<]+)}{
      eval{
            local $_=$1;
            unless (m{"}){
                s{^\s*}{"};
                s{\s*\Z}{" };
            }
            return $_;
          };
    }smge;

    ## Date:
    s{^Date:\s\K([^\n<]+)}{
      eval{
            local $_=$1;
            s{[A-Za-z]{3}, }{}ms;           ## drop day of week.
            s{(?:^| )\K0(\d)}{$1}msg;       ## drop leading zeroes.
            s{ \d\d:\d\d\K:00}{};           ## drop seconds if zero.
            s{ [\+-]\d\d\d\d\K[^\n]*}{};    ## drop trailing garbage.

            ## normalise TZ.
            s{ \K"?(UT|GMT)"?\Z}{+0000}sm;
            s{ \KPST\Z}{-0800}sm;
            s{ \KPDT\Z}{-0700}sm;
            s{ \K\-0000\Z}{+0000}sm;
            s{ \+0000\Z}{}sm;

            return $_;
          };
    }smge;

    ## squash spaces in References:
    s{^References:\s\K([^\n]+)}{
      eval{
            local $_=$1;
            s{\s+}{ }smg;

            return $_;
          };
    }smge;

    ## Leading space(s) anomaly
    s{^\s+(This is an OpenPGP/MIME signed message \(RFC 4880 and 3156\))}{$1}ms;

    return $_
}

sub transcod {
    local $_= shift;
    s{=\?([A-Za-z0-9\-]+)\?([BQ])\?(.+?)\?=}{       # (UTF\-8|KOI8\-R|WINDOWS\-1251|ISO\-8859\-1)
      eval{
            local $_=$3;
            my $codep=$1;
            my $encod=$2;
            if ($encod eq q{B}){
                $_ = decode_base64($_);
            }elsif($encod eq q{Q}){
                $_ = decode_qp($_);
            }
            my $iconv = Text::Iconv->new($codep, "UTF-8");
            if( my $s = $iconv->convert($_) ){
                $_ = $s;
            }
            else{
                ## invalid encoding, encode to base64:
                $s = encode_base64($_);
                chomp($s);
                $_ = "=?$codep?B?".$s.'?=';
            }
            return $_;
          };
    }smgei;
    return $_;
}

sub tests {
    use Test::More;

    my $nOK = q{From: "Some Person" <where@email.is>};
    isnt    filter($nOK),
            q{From: 1},
            'sanity check 1'
    ;
    isnt    filter($nOK),
            q{From:},
            'sanity check 2'
    ;
    isnt    filter($nOK),
            q{From: },
            'sanity check 3'
    ;
    isnt    filter($nOK),
            q{},
            'sanity check 4'
    ;

    is   sha256_base64(q{sha256_base64}),
         q{mHvpVXWmHmpFhLfeYJaaQgnU05NbHJdn7CTepU5f1LU},
    'sha256_base64'
    ;

    diag "\nContent-Type tests\n";

    my $OK=q{Content-Type: charset=UTF-8; name=abiword-2014march-rdf.patch; text/x-patch};

    $_ = $OK;
    is   filter($_),
         $OK,
    'same (correct) value'
    ;

    $_= q{Content-Type: text/x-patch; charset="UTF-8"; name="abiword-2014march-rdf.patch"};
    is   filter($_),
         $OK,
    'reorder fields'
    ;

    diag "\nEncoding tests\n";
    #~ is   filter(q{From: Hubert =?ISO-8859-1?Q?Figui=E8re?= <hfiguiere@teaser.fr>}),
         #~ q{From: Hubert =?utf-8?B?RmlndWnDqHJl?= <hfiguiere@teaser.fr>},
    #~ 'ISO-8859-1'
    #~ ;

    #~ is   filter(q{From: =?KOI8-R?B?+uHvIPLBysbGwcraxc7Cwc7L?= <raiffeisen@raiffeisen.ru>}),
         #~ q{From: =?utf-8?B?0JfQkNCeINCg0LDQudGE0YTQsNC50LfQtdC90LHQsNC90Lo=?= <raiffeisen@raiffeisen.ru>},
        #~ 'KOI8-R'
    #~ ;

    is   filter(q{From: Hubert =?ISO-8859-1?Q?Figui=E8re?= <hfiguiere@teaser.fr>}),
         q{From: Hubert Figuière hfiguiere@teaser.fr},
    'ISO-8859-1'
    ;

    is   filter(q{From: =?KOI8-R?B?+uHvIPLBysbGwcraxc7Cwc7L?= <raiffeisen@raiffeisen.ru>}),
         q{From: ЗАО Райффайзенбанк raiffeisen@raiffeisen.ru},
    'KOI8-R'
    ;

    is   filter(q{From: =?utf-8?B?0JfQkNCeINCg0LDQudGE0YTQsNC50LfQtdC90LHQsNC90Lo=?= <raiffeisen@raiffeisen.ru>}),
         q{From: ЗАО Райффайзенбанк raiffeisen@raiffeisen.ru},
    'UTF-8'
    ;

    is   filter(q{To: =?US-ASCII?Q?=C1=E0=E7=E0=F0=EE=E2_=C0=EB=E5=EA=F1=E0=ED=E4=F0?=}),
         q{To: =?US-ASCII?B?weDn4PDu4l/A6+Xq8eDt5PA=?=},
    'invalid encoding'
    ;

    done_testing();
    return;
}
